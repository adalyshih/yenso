var express = require('express');
var multer = require('multer');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var md5File = require('md5-file');
var mkdirp = require('mkdirp');
var jwt = require('jsonwebtoken');
var config = require('config.json')(path.join(__dirname, '../config.json'));
var Firebase = require("firebase");
var shortid = require('shortid');
var upstream=multer();


var app = express();

var fileId = '';
var storage = multer.diskStorage({

    destination: function (req, file, cb) {
        var dest = path.join(__dirname + '/../photo', req.params.pid);
        mkdirp.sync(dest);
        cb(null, dest);

    },

    filename: function (req, file, callback) {
        fileId = shortid.generate()
        callback(null, fileId + '.jpg');
    }
});

var upload = multer({ storage: storage }).single('userPhoto');

// router.post('/authenticate', function (req, res, next) {

// check the user have token
// console.log(req.headers['x-access-token']);
//有token就跳過
// if (req.headers['x-access-token']) {
//     next();
// } else {
//     if (req.auth.provider){
            
//     }      
// };
    
// var Firebase=new firebase(fbUrl);
    
// var authuser={};
 
// if user is found and password is right
// create a token
// var token = jwt.sign({ '123': 'name' }, config.yensosecret, {
//     expiresIn: 14400 // expires in 24 hours
// });

// return the information including token as JSON
// res.json({
//     success: true,
//     message: 'Enjoy your token!',
//     token: token
// });
  
//   User.findOne({
//     name: req.body.name
//   }, function(err, user) {

//     if (err) throw err;

//     if (!user) {
//       res.json({ success: false, message: 'Authentication failed. User not found.' });
//     } else if (user) {

//       // check if password matches
//       if (user.password != req.body.password) {
//         res.json({ success: false, message: 'Authentication failed. Wrong password.' });
//       } else {

//         // if user is found and password is right
//         // create a token
//         var token = jwt.sign(user, app.get('superSecret'), {
//           expiresInMinutes: 1440 // expires in 24 hours
//         });

//         // return the information including token as JSON
//         res.json({
//           success: true,
//           message: 'Enjoy your token!',
//           token: token
//         });
//       }   

//     }

//   });
// });

router.get('/', function (req, res) {
    //res.sendStatus(404).end();
    res.sendFile(path.join(__dirname + '/../public/main.html'));
});

router.post('/api/photo/:pid/:uid', function (req, res) {


    upload(req, res, function (err) {
        if (err) {
            return res.end("Error uploading file.");
        }

        var now = new Date().toISOString()
        // var abpath = path.join(__dirname, '/../photo', req.params.pid, fileId + '.jpg');
        var relpath = path.join('/../photo', req.params.pid, fileId + '.jpg');

        var myOptions = {
            'pid': req.params.pid,
            'uid': req.params.uid,
            'cId': req.body.cId,
            'iId': req.body.iId,
            // 'cSum': req.body.cSum,
            //'photoSize': req.file.size,
            'time': now,
            //'sSum': md5File(abpath),
            // 'ftoken': req.body.token,
            'imgpath': relpath,
        };
        console.log(myOptions);
        var fburl = "https://yenso.firebaseio.com/projects";

        var ref = new Firebase(fburl);


        ref.authWithPassword({
            "email": "adaly@tnet.net.tw",
            "password": "rfvnji"
        }, function (error, authData) {
            if (error) {
        console.log('error');
            } else {
                var onComplete = function (error) {
                    if (error) {
                        console.log('Synchronization failed');
                    } else {
                        console.log('Synchronization succeeded');
                    }
                };
            
                // var newitem = { 'checked': true, 'uptime': myOptions.time, 'filename': fileId + '.jpg' ,'name':''};
                var iRef=ref.child(myOptions.pid).child('cats').child(myOptions.cId).child('items').child(myOptions.iId);
                iRef.child('checked').set(true);                   
                iRef.child('uptime').set(myOptions.time);
                iRef.child('filename').set(fileId + '.jpg');
                
            }
        });




        var data = JSON.stringify(myOptions);

        fs.writeFile(path.join(__dirname + '/../public/uploads/test.json'), data, function (err) {
            if (err) {

                return;
            }

        })
        res.json({status: "ok"});
    });
});

router.post('/api/register', upstream.array(), function (req, res) {
var ref = new Firebase("https://yenso.firebaseio.com");
    switch (req.body.provider) {

        case "facebook":
            
            ref.authWithOAuthToken("facebook", req.body.token, function (error, authData) {
                if (error) {
                    res.json({ status: 'TOKEN 無效' })
                } else {
                    ref.once("value", function (snapshot) {
                        var a = snapshot.child('users').child(authData.uid).exists()
                        if (a) {
                            res.json({ status: "已有此帳號" })
                        }
                        else {
                            console.log('user not exist')
                            ref.child('users').child(authData.uid).set({
                                'name': authData.facebook.displayName,
                                'email': authData.facebook.email,
                                'credit': 1,
                                'status': "normal"
                            }, function () {
                                res.json({ status: "創建帳號成功" })
                            })
                        }
                    })
                }
            });
            break;
            
        case "email":
        
            ref.createUser({
                email: req.body.email,
                password: req.body.pwd
            }, function (error, userData) {
                if (error) {
                    switch (error.code) {
                        case "EMAIL_TAKEN":
                            res.json({ status: "已有此帳號" })
                            break;
                        case "INVALID_EMAIL":
                            res.json({ status: "郵件格式有誤" })
                            break;
                        default:
                            res.json({Error:error});
                    }
                } else {
                    console.log(userData);
                    ref.child('users').child(userData.uid).set({
                        'name': req.body.name,
                        'email': req.body.email,
                        'credit': 1,
                        'status': "normal"
                    })
                     res.json({ status: "創建帳號成功" })    
                   
                }
            });
            break;
        default:
            res.json({ status: '請提供帳號種類' })
            break;
}


});
// router.get('/show', function (req, res) {


//     var tmp = JSON.parse(fs.readFileSync(path.join(__dirname + '/../public/uploads/test.json')));

//     res.render('index', {
//         title: '驗收',
//         pid: tmp.pid,
//         uid: tmp.uid,
//         cName: tmp.cName,
//         iName: tmp.iName,
//         cSum: tmp.cSum,
//         photoSize: tmp.photoSize,
//         time: tmp.time,
//         sSum: tmp.sSum,
//         imgpath: tmp.imgpath,
//         ftoken: tmp.ftoken
//     });
// });






// router.get('/show/:pid/:cname/:iname', function (req, res) {

//     res.sendfile(path.join(__dirname, '/../photo', req.params.pid, fileId + '.jpg'))

// });
/* GET home page. */
// router.get('/', function(req, res, next) {
//  
//   res.sendFile(path.join(__dirname + '/../public/login.html'));
// });
// 
// router.post('/api/photo',function(req,res){
//     upload(req,res,function(err) {
//         if(err) {
//             return res.end("Error uploading file.");
//         }
//         res.end("File is uploaded");
//     });
// });

module.exports = router;