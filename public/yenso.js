
var app = angular.module("yenso", ["firebase", 'ngPrettyJson', 'ngRoute','toggle-switch','ui.bootstrap',"xeditable",'ui-notification']);

app.factory("Auth", ["$firebaseAuth",
    function ($firebaseAuth) {
        var ref = new Firebase("https://yenso.firebaseio.com");
        return $firebaseAuth(ref);
    }
]);


    
app.config(function ($routeProvider) {
    $routeProvider
        .when('/login', {
            controller: 'loginUser',
            templateUrl: 'login.html'
        })
        .when('/users/:uid', {
            controller: 'yensoUser',
            templateUrl: 'yensouser.html'
        })
        .when('/users/:uid/:pid', {
            controller: 'yensoUser',
            templateUrl: 'yensouser.html'
        })

        .when('/projects', {
            controller: 'viewProjects',
            templateUrl: 'viewprojects.html'
        })
        .otherwise({
            controller: 'loginUser',
            templateUrl: 'login.html'
        });
});

app.controller("viewProjects", ['$scope', "Auth", '$location',
    function ($scope, Auth, $location) {
        $scope.authObj = Auth;
        $scope.authObj.$onAuth(function (authData) {
            if (authData) {
                $scope.uid = authData.uid;
                console.log("Logged in as:", authData.uid);
            } else {
                $location.path("/login")
                console.log("Logged out");
            }
        });
    }
]);
// app.controller("navBar",['$scope',"Auth",'$location',function($scope,Auth,$location){
//      $scopeAuthobj=Auth
//     // var authData = Auth.$getauth();
//     console.log();
//     $scope.unAuth=function(){
//        $scope.authobj.$unauth();
//     };
// }]);

app.controller("yensoUser", ['$scope', "Auth", '$location', '$firebaseObject', '$firebaseArray', '$routeParams','Notification',
    function ($scope, Auth, $location, $firebaseObject, $firebaseArray, $routeParams, Notification) {
        $scope.newPname = '';
        $scope.editProject = {};
        $scope.newPfield = true;
        $scope.authObj = Auth;
        $scope.itemDone=0;
        $scope.itemTotal=0;
        $scope.limit=true;
        $scope.inputItem={};
        $scope.inputItem.text="";
        $scope.inputItem.share="";
        $scope.editProject.nowcat="";
        
        
        //依params載入專案及user清單
        $scope.authObj.$onAuth(function (authData) {
            if (authData) { 
                
                $scope.uid = authData.uid;
                var fbURL = "https://yenso.firebaseio.com/" ;
                var uRef=new Firebase(fbURL+ "users/" + authData.uid)               
                var objuser = $firebaseObject(uRef);
                var userProjects = $firebaseArray(uRef.child('projects'));              
                if ($routeParams.pid) {
                   var pRef=new Firebase(fbURL+"projects").child($routeParams.pid);                  
                   var objEditProject = $firebaseObject(pRef);
                   objEditProject.$bindTo($scope,'editProject').then(function(){                     
                       if ($scope.editProject.projectName || null)  {  
                           Notification.warning('專案列表載入成功!');
                                                       
                      }
                        else 
                        {
                           
                              //專案不存在
                       $location.path("/users" + authData.uid)
                         };             
                   });}
                                            
                objuser.$loaded()
                    .then(function (x) {
                        $scope.uName = objuser.name;
                    })
                    .catch(function (error) {
                        console.log("Error:", error);
                    });
                    
                userProjects.$loaded()
                    .then(function (x) {

                        $scope.pLists = userProjects;
                    })
                    .catch(function (error) {
                        console.log("Error:", error);
                    });
         
            } else {
                $location.path("/login")
            }
        });
        //增加專案人員
        $scope.addShare=function () {
            //email to UID
                   var uEmail= $scope.inputItem.share;
                var fbURL = "https://yenso.firebaseio.com/" ;
                var uRef=new Firebase(fbURL+'users') ;    
                uRef.orderByChild("email").equalTo(uEmail).once("value", function(snapshot) {
                   // $scope.test=snapshot;
                 var obj=  snapshot.exportVal();
                   //取得此email uid
                  var addUid=Object.keys(obj)[0];
                  var addUname=obj[Object.keys(obj)[0]].name;
                  var addUref=new Firebase(fbURL).child('users').child(addUid).child('projects');
                  addUref.child($routeParams.pid).set($scope.editProject.projectName);
                  
               var addPref=new Firebase(fbURL).child('projects').child($routeParams.pid).child('sharelists');
               addPref.child(addUid).set(addUname);
                  Notification.warning('已與' + addUname + '分享此專案')
                  $scope.inputItem.share='';
                                     
});
                          
                // $firebaseObject(uRef.child('projects').child(id)).$remove().then(function () {
                //     $firebaseObject(uRef.parent().parent().child('projects').child(id)).$remove();
                // })
        };
        //減少專案人員
                $scope.removeShare=function (uid,uname) {
                var fbURL = "https://yenso.firebaseio.com/" ;
                var uRef=new Firebase(fbURL+'users') ;
                  var pRef =new Firebase(fbURL+'projects'); 
                $firebaseObject(uRef.child(uid).child("projects").child($scope.editProject.$id)).$remove().then(
                    function(){
                            $firebaseObject(pRef.child($scope.editProject.$id).child('sharelists').child(uid)).$remove();
                            Notification.warning('已將' + uname + '移出此專案');
                    }
                )  
                
        };
        
        
       //移除專案清單

        $scope.removeP=function (id){
                
               var fbURL = "https://yenso.firebaseio.com/" ;
                var uRef=new Firebase(fbURL+ "users/" + $scope.uid);               
                $firebaseObject(uRef.child('projects').child(id)).$remove().then(function () {
                    //先刪專案share
                 var pRef= new Firebase(fbURL+ "projects/" + id).child('sharelists');
                 var pName=pRef.parent().name;
                 var share=0;
                 pRef.once("value",function(snapshot){
                     share=snapshot.numChildren();
                     if(snapshot.numChildren()==0){pRef.parent().remove();
                     Notification.error('專案已刪除')
                     
                     }//直接刪專案}
                     
                     //取得有分此專案的uid將其下projects刪除
                     else{
                            snapshot.forEach(function(childSnapshot){
                            var pUid=childSnapshot.key();
                            var pUname=childSnapshot.val();
                            var uPRef=new Firebase(fbURL+ "users/" + pUid);
                            console.log(share);


                            uPRef.child('projects').child(id).remove();
                            share=share-1;
                            if(share == 0){
                                    pRef.parent().remove();
                                    Notification.error('專案已刪除')
                                };
                            Notification.warning('已將'+pUname+'移出')
                         });
                     }
                 });
                 
                })
               
        };
        
        //創建項目及cat
        $scope.createItem=function(){
            
                var fbURL = "https://yenso.firebaseio.com/" ;
                var uRef=new Firebase(fbURL+ "projects/" + $scope.editProject.$id) 
                
                
          if ($scope.editProject.nowcat){
                //創項目
                $firebaseArray(uRef.child('cats').child($scope.editProject.nowcat).child('items'))
                .$add({'name':$scope.inputItem.text,'checked':false,'filename':""});
                Notification.success($scope.inputItem.text +'   項目已增加');
                $scope.inputItem=""
                
                }
          else{
              $firebaseArray(uRef.child('cats')).$add({'name':$scope.inputItem.text})
              Notification.success($scope.inputItem.text +'   類別已增加');
              $scope.inputItem=""
          };    
        };
        
        $scope.removeItem=function(a){

                 Notification.error($scope.editProject.cats[$scope.editProject.nowcat]['items'][a].name + '   項目已刪除');
               delete $scope.editProject.cats[$scope.editProject.nowcat]['items'][a];


        };
                $scope.removeCat=function(key){
                    
                    Notification.error($scope.editProject.cats[key].name + '   類別已刪除');
                delete $scope.editProject.cats[key];

        };

        // //創建cat
        // $scope.createCat=function(){
            
            
        // }
            //  var fbURL = "https://yenso.firebaseio.com";
            //  var ref = new Firebase(fbURL).child('projects').child($scope.editProject.$id).child('cats');
            // //  var list=$firebaseArray(ref);
            // var newname="新類別";
            // var basename="新類別";
            // var incre=1;
            
            // var list={};

            // //亂 新增檔名的 要改
            // ref.once('value', function(snap) { 
            //                 list = snap.val(); 
                                     
            //                  if (!(list)){                     
            //                      var refs=ref.child(newname);
            //                      var initcat={'total':0};
            //                      refs.set(initcat);
            //                      Notification.info('修改已儲存');}
            //                   else if (!list.hasOwnProperty(newname)){
                                 
            //                      var initcat={'total':0};
                                 
            //                      ref.child(newname).set(initcat);}
                                 
                              
            //                     else {
            //                     while (list.hasOwnProperty(newname)){
                                    
            //                         newname=basename+incre;                                   
            //                         incre  = incre+1;
            //                         if   (!list.hasOwnProperty(newname)){
            //                             var refs=ref.child(newname);
            //                             var initcat={'total':0};
            //                             refs.set(initcat);
            //                             Notification.info('修改已儲存');
            //                             break;
            //             }              
            //            }; 
            //            }; 
            // });  
            //}            

        
        //創建project
        $scope.createProject = function () {
            
            var fbURL = "https://yenso.firebaseio.com";
            var ref = new Firebase(fbURL).child('projects');
            var list = $firebaseArray(ref);
            var newP = {
                projectName: $scope.newPname,
                creatorId: $scope.uid,
                creatorName: $scope.uName,
                createTime: Firebase.ServerValue.TIMESTAMP,
            };
          

            list.$add(newP).then(function (ref) {
                $scope.newPname="";
                var pid = ref.key();
                var userRef = new Firebase(fbURL).child('users').child($scope.uid).child('projects').child(pid);
                userRef.set(newP.projectName);
                    $location.path("/users/" + $scope.uid + "/" + pid);
                
            });
        };    
    }
]);


app.controller("loginUser", ['$scope', "Auth", '$location', '$firebaseArray',
    function ($scope, Auth, $location, $firebaseArray) {
        
        $scope.signupView=false;
        $scope.errPwd = false;
        $scope.authObj = Auth;
        $scope.authObj.$onAuth(function (authData) {
            if (authData) {
 
                $location.path("/users/" + authData.uid)
            }
        });
        
        $scope.createUser = function () {
            $scope.authObj.$createUser({
                email: $scope.createEmail,
                password: $scope.createPw
            }).then(function (userData) {

                var fbURL = "https://yenso.firebaseio.com";
                var obj = new Firebase(fbURL);
                
                //初始化使用者資料
                    
                var profile = {
                    'name': $scope.createName,
                    'email': $scope.createEmail,
                    // 'flist': [''],
                    // 'projectsId': [''],
                    'credit': 1,
                    'status': "normal"
                };
                obj.child('users').child(userData.uid).set(profile);
                $location.path("/users/" + userData.uid);
            }).catch(function (error) {
                console.error("Error: ", error);
            });
        };

        $scope.yLogin = function () {
            $scope.errPwd = false;
            $scope.authObj.$authWithPassword({
                email: $scope.email,
                password: $scope.pass

            }).then(function (authData) {
                var fbURL = "https://yenso.firebaseio.com";
                var obj = new Firebase(fbURL);
                var list = $firebaseArray(obj.child('users'));
                var rec = list.$getRecord(authData.uid);
                                
                //初始化使用者資料
                if (angular.isDefined(rec)) {
                    
                } else {
                    var profile = {
                        'name': authData.facebook.displayName,
                        'email': authData.facebook.email,
                        // 'flist': [''],
                        // 'projectsId': [''],
                        'credit': 1,
                        'status': "normal"
                    };
                    obj.child('users').child(authData.uid).set(profile);
                    //初始化使用者資料
                };
                
               $location.path("/users/" + authData.uid);
                

            }).catch(function (error) {
                switch (error.code) {
                    case "INVALID_EMAIL":
                        console.log("The specified user account email is invalid.");
                        break;
                    case "INVALID_PASSWORD":
                        $scope.errPwd = true;
                        console.log("The specified user account password is incorrect.");
                        break;
                    case "INVALID_USER":
                    $scope.errPwd = true;
                        console.log("The specified user account does not exist.");
                        break;
                    default:
                        console.log("Error logging user in:", error);
                }
            });
        };
        

        $scope.fbLogin = function () {
            $scope.authObj.$authWithOAuthPopup("facebook", {
                scope: "email"
            }).then(function (authData) {
                
                var fbURL = "https://yenso.firebaseio.com";
                var obj = new Firebase(fbURL);
                var list = $firebaseArray(obj.child('users'));
                
                  list.$loaded().then(function(){
                   var rec = list.$getRecord(authData.uid);
                   if (rec || null) {
                    
                } else {
                    var profile = {
                        'name': authData.facebook.displayName,
                        'email': authData.facebook.email,
                        'credit': 1,
                        'status': "normal"
                    };
                    obj.child('users').child(authData.uid).set(profile);                              
                    //初始化使用者資料
                }; 
                  });                   
            }).catch(function (error) {
                switch (error.code) {
                    case "INVALID_EMAIL":
                        console.log("The specified user account email is invalid.");
                        break;
                    case "INVALID_PASSWORD":
                        console.log("The specified user account password is incorrect.");
                        break;
                    case "INVALID_USER":
                        console.log("The specified user account does not exist.");
                        break;
                    default:
                        console.log("Error logging user in:", error);
                }
                console.error("Authentication failed:", error);
            });
        };
    }



]);
